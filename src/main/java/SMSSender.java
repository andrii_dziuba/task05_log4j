import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;

public class SMSSender {

    public static final String ACCOUNT_SID = "AC7de04e485fbb8f00b19f271a3156524e";
    public static final String AUTH_TOKEN = "7c683257f67c0e1f8a9bda2fe883f599";

    static {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    public static void send(String mes) {
        MessageCreator message = Message.creator(new PhoneNumber("+380507389681"),
                new PhoneNumber("+18608502018"), mes);
        message.create();
    }

}
