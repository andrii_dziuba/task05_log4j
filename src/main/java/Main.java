import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main implements Runnable {

    public static final Logger logger = LogManager.getLogger(Main.class);

    private ScheduledExecutorService executorService = null;

    Main() {
        executorService = Executors.newSingleThreadScheduledExecutor();
    }

    public static void main(String[] args) {
        logger.debug("Dutka");
//        new Main().start();
    }

    /**
     * Begins an execution of <code>MainTimer</code>.
     *
     * @see #run()
     */
    public void start() {
        executorService.scheduleAtFixedRate(this, 500, 5000, TimeUnit.MILLISECONDS);
    }

    @Override
    public void run() {
        LogMaker.randomLog(this.getClass());
    }
}
