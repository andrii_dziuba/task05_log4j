import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;

public class LogMaker {

    public static Logger logger = null;

    /**
     * Creates a log with random level for class <code>cl</code>
     * @see #randomLogLevel()
     * @param cl
     */
    public static void randomLog(Class cl) {
        if(logger == null) {
            logger = LogManager.getLogger(cl);
        }
        Level level = randomLogLevel();
        logger.log(randomLogLevel(), randomLogLevel() + " from " + cl);
    }

    /**
     * Returns a random <code>Level</code> of all available in class <code>Level</code>
     * @return
     */
    private static Level randomLogLevel() {
        Level[] levels = Level.values();
        int random = ThreadLocalRandom.current().nextInt(levels.length);
        return levels[random];
    }

}
